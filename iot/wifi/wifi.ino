#include <SoftwareSerial.h>
#include <HardwareSerial.h>
class Wifi{
public:
    Wifi(int RX,int SX,int speed = 9600){
        wifi = new SoftwareSerial(RX,SX);
        wifi->begin(speed);
    }
    String TestWifi(){
        wifi->println("AT");
        readTime(2);
        return readLine();
    }
    String SetMulti(){
        //AT+CIPMUX=1 
        wifi->println("AT+CIPMUX=1");
        readTime(2);
        return readLine();
    }
    String StartServer(){
        //AT+CIPSERVER=1
        wifi->println("AT+CIPSERVER=1");
        readTime(1);
        String ret = readLine();
        if(ret == "no change")
            return readTime(1),readLine();
        else return ret;
    }
    String GetIP(String & APIP,String & STAIP){
        //AT+CIFSR
        /*
+CIFSR:APIP,"192.168.4.1"
+CIFSR:APMAC,"5e:cf:7f:f6:25:9a"
+CIFSR:STAIP,"192.168.43.89"
+CIFSR:STAMAC,"5c:cf:7f:f6:25:9a"
        */
        wifi->println("AT+CIFSR");
        readTime(1);
        String ret;
        int cnt=0;
        while ((ret = readLine()).length()>1)
        {
            cnt++;
            if(cnt==1){
                APIP = ret.substring(13,ret.length()-1);
            }else if(cnt==3){
                STAIP = ret.substring(14,ret.length()-1);
            }
        }
        return readLine();
    }
    bool PollMessage(String & content){
        //+IPD,0,7:
        String ret = wifi->readStringUntil('\n');
        if(ret.length()>0){
            if(ret.substring(0,4)=="+IPD"){
                content = ret.substring(ret.indexOf(':')+1,ret.length());
                //Serial.println(content);
                return true;
            }
            else PutNormal(ret);
        }
        return false;
    }
    void Debug(){
        String ret = wifi->readStringUntil('\n');
        if(ret.length()>0){
            PutNormal(ret);
        }
    }
    SoftwareSerial * getHandle(){
        return wifi;
    }
private:
    SoftwareSerial *wifi;
    void readCount(int cnt){
        while(cnt>0)
            cnt -= wifi->readStringUntil('\n').length();
    }
    void readTime(int cnt){
        while (cnt>0)
            if(wifi->readStringUntil('\n').length()>0)cnt--;
    }
    String readLine(bool deleteEnd = true){
        String ret = wifi->readStringUntil('\n');
        if(ret.length()>0){
            if(deleteEnd)ret.remove(ret.length()-1);
            return ret;
        }
    }
    void PutNormal(String msg,bool Len = true){
        Serial.print(msg.length());
        if(Len)Serial.print("#"),Serial.println(msg);
        else Serial.print("\n");
    }
};
Wifi wifi(10,11);
int LEDPIN = 6;
bool LEDSTATE = false;
void setup(){
    Serial.begin(9600);
    pinMode(LEDPIN,OUTPUT);
}
void loop(){
    static bool hassend = true;
    static bool restful = false;
    static String ip1,ip2,content;
    static int ret;
    if(hassend){
        hassend=false;
        Serial.println(wifi.TestWifi());
        Serial.println(wifi.SetMulti());
        Serial.println(wifi.StartServer());
        Serial.println(wifi.GetIP(ip1,ip2));
        Serial.println(ip1);
        Serial.println(ip2);
        
    }
    restful = wifi.PollMessage(content);
    if(restful){
        Serial.println(content);
        if(content == "O")LEDSTATE = true;
        if(content == "F")LEDSTATE = false;
    }
    if(LEDSTATE)digitalWrite(LEDPIN,1);
    else digitalWrite(LEDPIN,0);
    if (false && Serial.available()) {
        wifi.getHandle()->write(Serial.read());
    }
    ret = Serial.read();
    if(ret == 79)LEDSTATE = true;
    if(ret == 70)LEDSTATE = false;
}