#include <agentFactory/agentFactory.h>
int main(){
    shared_ptr<Agent> agent = AgentFactory::build();
    agent->Init();
    while(!agent->ShouldClose()){
        agent->Event();
        agent->Clear();
        agent->Update();
    }
    return 0;
}