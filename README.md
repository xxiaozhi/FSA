# FSA - Future Smart Assistant
> 未来智能助理
一款以全新的人机交互方式提供服务的人工智能软件
能够以语音和GUI等方式作为输入进行控制操作系统和IoT设备
同时有一个可以选择的三维人形作为前端
基于OpenGL，提供IAgentComponent接口，可以开发更多的自定义功能组件

# 技术细节
- 使用组合实体模式，模板模式，工厂模式，单例模式，观察者模式等
- 为组件开发提供一个多线程安全的消息队列用于数据交换和事件触发

# 视频演示

[点我看演示视频1](https://www.bilibili.com/video/av49910144/ "点我看演示视频1")

[点我看演示视频2](https://www.bilibili.com/video/av46463021/ "点我看演示视频2")

图片我就不上传了，挺占用资源的，一张图片几M，网速不好能等好几秒

# 所用到的库有
- jsoncpp
- curl-openssl
- crypto
- pthread
- glfw3
- gl3w
- saba
- aip-cpp
- imgui
- msc
- aiui
- ...

# 编译开发运行
如果使用Ubuntu，请sudo apt install

libjsoncpp-dev libcurl-openssl1.0-dev libbullet-dev libglfw3-dev libcurl4-openssl-dev libxinerama-dev libxcursor-dev libasound2-dev libxi-dev libssl-dev sox

其他发行版类似，我已经在Manjaro（Arch），Ubuntu上成功编译运行。

如果要移植到Windows，录音和播放功能要重写。未来可能考虑用QT，免除编写跨平台代码的烦恼。

项目使用cmake，请先熟悉cmake的使用方法。等我有时间出个工具链的详细教学视频。

万一你编译成功了，记得复制resources文件夹到build目录下。

# 所用到的开源项目
https://github.com/benikabocha/saba.git

https://github.com/ocornut/imgui.git

[点我看开发记录](http://note.youdao.com/noteshare?id=5553ead8daf10592b5a47c29ac63c980 "点我看开发记录")

|  支持的操作系统 | 适配情况 |
| :------------: | :------------: |
| Windows  | False |
|  Linux | True |

# 已知问题
- 由于opengl的特殊性，不允许多线程调用opengl函数，目前的Init是异步执行的，会影响效率。
- 语音实时识别的录音模块和语音播放模块会小概率出现线程死锁问题，正在解决
- glfw的透明化窗口的透明部分不支持鼠标穿透，且占用CPU严重，在某些x11桌面环境下不支持透明化

# 开源许可
基于或修改本项目的所有软硬件都需要开放其源代码。

# Tips
- 本作品参加计算机创新设计大赛仅获三等奖，于是开源出来了。
- 代码中有供测试的科大讯飞语音appid，使用次数有限。
- 如果有志同道合共同讨论技术的，可以加QQ群：221359737。本人是学生党。
- 如果有人能帮我用QT重写项目当然更好了，提供点技术支持也好啊。
- 能给个Star？？？