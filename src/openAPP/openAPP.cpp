#include <openAPP/openAPP.h>
OpenAPP::OpenAPP():
    IAgentComponent(AgentComponentType::GUI,false),
    Config("resource/config/app.json"){
    
}
void OpenAPP::Init(){
    Json::Value root = getRoot();
    vector<string> names = root.getMemberNames();
    for (auto &&i : names)
    {
        links[i] = getValue(i).asString();
    }
}
void OpenAPP::PollMessage(Message message){
    if(message.fromType == AgentComponentType::NLP){
        string type = message.content.at(0);
        if(type == "app"){
            Json::Reader reader;
            Json::Value root;
            reader.parse(message.content.at(1),root,false);
            root = root["intent"]["semantic"][0]["slots"][0]["value"];
            cout<<root.asString()<<endl;
            if(links[root.asString()] != "")system(links[root.asString()].c_str());
        }
    }
}