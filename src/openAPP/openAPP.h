#pragma once
#include <agent/agent.h>
#define IMGUI_IMPL_OPENGL_LOADER_GL3W
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
class OpenAPP: public IAgentComponent,Config{
public:
    OpenAPP();
    void PollMessage(Message message);
    void Init();
private:
    map<string,string> links;
};