#pragma once
#include <agent/agent.h>
#define IMGUI_IMPL_OPENGL_LOADER_GL3W
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <qisr.h>
#include <qtts.h>
#include <msp_cmn.h>
#include <msp_errors.h>
#include <iat/speech_recognizer.h>
#include <functional>
#include <unistd.h>
class Iat: public IAgentComponent,Config{
public:
    Iat();
    ~Iat();
    void Init();
    void BeforeEnd();
    void PollMessage(Message message);
    void Update();
    void StartRecord();
    void EndRecord();
	void TextToSpeech_Async(string text,string filePath = "resource/tmp/tts_xfly.wav");
	bool TextToSpeech_Sync(string text,string filePath = "resource/tmp/tts_xfly.wav",bool endLast = true);
private:
    void SendMessage(string type,string result);

    static void on_result(const char *result, char is_last);
    static void on_speech_begin();
    static void on_speech_end(int reason);
    speech_rec iat;
    speech_rec_notifier recnotifier = {
		on_result,
		on_speech_begin,
		on_speech_end
	};

    const char* login_params = "appid = 5c223f6c, work_dir = ./resource";
    const char* session_begin_params_stt =
		"sub = iat, domain = iat, language = zh_cn, "
		"accent = mandarin, sample_rate = 16000, "
		"result_type = plain, result_encoding = utf8";
	const char* session_begin_params_tts = "voice_name = xiaoyan, text_encoding = utf8, sample_rate = 16000, speed = 50, volume = 50, pitch = 50, rdn = 2";
    bool panel_show = true;
    bool record_ok = true;

	bool startWhenEnd = true;
    bool quiet = true;

    string message_sum;

    static Iat * m_instance;

    thread thd_tts;
    bool tts_ok = true;
};