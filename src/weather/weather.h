#pragma once
#include <agent/agent.h>
#define IMGUI_IMPL_OPENGL_LOADER_GL3W
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <base/http.h>
#include <mmd/mmd.h>
class Weather :public IAgentComponent,Config{
public:
    Weather();
    ~Weather(){};
    void PollMessage(Message message);
    void Update();
private:
    bool show_panel = false;
    vector<map<string,string> > weather_result;
    map<int,GLuint> idToImg;
    aip::HttpClient client;
    mutex mutex_res;
private:
    void ParseWeather(string content);
};