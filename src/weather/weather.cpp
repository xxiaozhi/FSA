#include <weather/weather.h>
Weather::Weather():
    Config("resource/config/weather.json"),
    IAgentComponent(AgentComponentType::GUI,false){
    
}

void string_replace( std::string &strBig, const std::string &strsrc, const std::string &strdst)
{
    std::string::size_type pos = 0;
    std::string::size_type srclen = strsrc.size();
    std::string::size_type dstlen = strdst.size();

    while( (pos=strBig.find(strsrc, pos)) != std::string::npos )
    {
        strBig.replace( pos, srclen, strdst );
        pos += dstlen;
    }
}

void Weather::ParseWeather(string content){
    weather_result.clear();
    Json::Value root;
    Json::Reader reader;
    string_replace(content,"℃","摄氏度");
    reader.parse(content,root,false);
    root = root["intent"]["data"]["result"];
    int cnt = root.size();
    mutex_res.lock();
    for(int i=0;i<cnt;++i){
        map<string,string> sub_item;
        vector<string> names = root[i].getMemberNames();
        for (auto &&name : names)
        {
            Json::Value tmp = root[i][name];
            if(tmp.isInt())sub_item[name] = to_string(tmp.asInt());
            else if(tmp.isString())sub_item[name] = tmp.asString();
            else if(tmp.isNumeric())sub_item[name] =to_string(tmp.asFloat());
            if(name == "img"){
                static int count_img = 0;
                string ret;
                client.get(sub_item[name],nullptr,nullptr,&ret);
                sub_item[name] = "resource/tmp/"+to_string(++count_img)+".png";
                ofstream ofs(sub_item[name]);ofs<<ret;
                //Texture tex =  AppContext::getInstance()->GetTexture(sub_item[name]);
                //can't load the texture here because it's multithread unsafe
            }
        }
        weather_result.push_back(sub_item);
    }
    mutex_res.unlock();
    show_panel = true;
    agent->PushMessage(Message(AgentComponentType::GUI,vector<string>{"PANEL_ON"}));
}
void Weather::PollMessage(Message message){
    if(message.fromType == AgentComponentType::NLP)
        if(message.content.at(0) == "weather"){
            thread thd(&Weather::ParseWeather,this,message.content.at(1));
            thd.detach();
        }
}
void Weather::Update(){
    if(!show_panel)return ;
    ImGui::Begin("天气");
    ImGui::LabelText("空气质量",weather_result[0]["airQuality"].c_str());
    ImGui::LabelText("湿度",weather_result[0]["humidity"].c_str());
    ImGui::LabelText("PM2.5",weather_result[0]["pm25"].c_str());
    mutex_res.lock();
    for (auto &&weather : weather_result)
    {
        if (ImGui::CollapsingHeader(weather["date"].c_str())){
            /*
            for (auto &&item : weather)
            {
                if(item.first == "img"){
                    ImGui::Image((GLuint *)(AppContext::getInstance()->GetTexture(item.second,false).m_texture),ImVec2(100.0f,100.0f));
                }else {
                    ImGui::LabelText(item.first.c_str(),item.second.c_str());
                }
            }
            */
            ImGui::Image((GLuint *)(AppContext::getInstance()->GetTexture(weather["img"],false).m_texture),ImVec2(128.0f,128.0f));
            ImGui::SameLine();
            ImGui::Text(weather["date_for_voice"].c_str());
            //ImGui::SameLine();
            ImGui::Text(weather["weatherDescription"].c_str());
            ImGui::LabelText("温度范围",weather["tempRange"].c_str());
            ImGui::LabelText("天气",weather["weather"].c_str());
            ImGui::LabelText("风速",weather["wind"].c_str());
        }
    }
    mutex_res.unlock();
    if(ImGui::Button("关闭"))show_panel = false;
    ImGui::End();
}