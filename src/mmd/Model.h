#pragma once 
#include <mmd/mmd.h>

class Model
{
public:
	//~Model();

	shared_ptr<saba::MMDModel>	m_mmdModel;
	shared_ptr<saba::VMDAnimation>	m_vmdAnim;

	GLuint	m_posVBO = 0;
	GLuint	m_norVBO = 0;
	GLuint	m_uvVBO = 0;
	GLuint	m_ibo = 0;
	GLenum	m_indexType;

	GLuint	m_mmdVAO = 0;
	GLuint	m_mmdEdgeVAO = 0;
	GLuint	m_mmdGroundShadowVAO = 0;

	vector<Material>	m_materials;

	bool Setup(AppContext& appContext);
	void Clear();

	void UpdateAnimation(const AppContext& appContext);
	void Update(const AppContext& appContext);
	void Draw(const AppContext& appContext);
        
	void setTranslate(const glm::vec3& t);
	
	glm::vec3	m_translate;
	bool m_loop;
	float m_begin_anim_time;
	//bool has_setup = false;
};