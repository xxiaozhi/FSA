#pragma once
#include <agent/AgentComponent.h>
#include <list>
#include <vector>
#include <iostream>
#include <mutex>
using namespace std;
class Message{
public:
    AgentComponentType fromType;
    vector<string> content;
    Message(){};
    Message(AgentComponentType fromType,vector<string> content);
private:
};
class MessageQueue{
public:
    MessageQueue();
    void PushMessage(Message message);
protected:
    list<Message> messageList;
    mutex mutex_messageList;
};