#include <messageQueue/messageQueue.h>
MessageQueue::MessageQueue(){

}
Message::Message(AgentComponentType fromType,vector<string> content){
    this->content = content;
    this->fromType = fromType;
}
void MessageQueue::PushMessage(Message message){
    mutex_messageList.lock();
    messageList.push_back(message);
    mutex_messageList.unlock();
}