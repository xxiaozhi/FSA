#pragma once
#include <agent/agent.h>
#define IMGUI_IMPL_OPENGL_LOADER_GL3W
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <speech.h>
class Baidu : Config, public IAgentComponent{
public:
    enum class DevPid{
        NormalWithEng=1536,
        NormalChi=1537,
        English=1737,
        ELanguage=1637,
        SiChuan=1837,
        NormalAway=1936
    };
    Baidu();
    void Init();
    void Update();
    void PollMessage(Message message);
    void SpeechToText_Async(string speechPath);
    void TextToSpeech_Async(string text,string savePath = "resource/tmp/tts.mp3");
    bool SpeechToText_Sync(string speechPath,bool endLast = true);
    bool TextToSpeech_Sync(string text,string savePath = "resource/tmp/tts.mp3",bool endLast = true);
private:
    void ShowPanel();
    void SendMessage(string type,string result);
private:
    const string app_id = "15954186";
    const string api_key = "CS2SRdBdZZNZwSntDKmXqOQm";
    const string secret_key = "EF3MqQ7dRSYR5OnxFDV5HEw8Uqukj4zb";
    aip::Speech * client;
    int m_DevPid = 1536;
    int m_speed = 5;
    int m_pitch = 5;
    int m_volume = 5;
    int m_person = 0;
    string m_audioType = "wav";
    int m_rate = 16000;

    bool GUILoadOK = false;
    bool ui_show = true;

    thread thd_tts,thd_stt;
    bool tts_ok = true,stt_ok = true;
};