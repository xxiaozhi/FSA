#include <baidu/baidu.h>
Baidu::Baidu(): 
    Config("resource/config/baidu.json"),
    IAgentComponent(AgentComponentType::SPEECH,false),
    client(new aip::Speech(app_id,api_key,secret_key)){
    
}
void Baidu::SpeechToText_Async(string speechPath){
    string file_content;
    //system("rec -r 16000 -c 1 -b 16 test.wav trim 0 00:10");
    aip::get_file_content(speechPath.c_str(), &file_content);
    map<string, string> options;
    options["dev_pid"] = to_string(m_DevPid);
    Json::Value result = client->recognize(file_content, m_audioType, m_rate, options);
    
    SendMessage("STTOK",result.toStyledString());
    stt_ok = true;

    //return result.toStyledString();
}
void Baidu::SendMessage(string type,string result){
    vector<string> content = {type,result};
    agent->PushMessage(Message(AgentComponentType::SPEECH,content));
}
void Baidu::TextToSpeech_Async(string text,string savePath){
    std::ofstream ofile;
    std::string file_ret;
    std::map<std::string, std::string> options;
    options["spd"] = to_string(m_speed);
    options["per"] = to_string(m_person);
    options["pit"] = to_string(m_pitch);
    options["vol"] = to_string(m_volume);
    ofile.open(savePath, std::ios::out | std::ios::binary);
    Json::Value result = client->text2audio(text, options, file_ret);
    if (!file_ret.empty())
    {
        ofile << file_ret;
    } else {
        std::cout << result.toStyledString();
    }
    SendMessage("TTSOK",savePath);
    tts_ok = true;
}
bool Baidu::SpeechToText_Sync(string speechPath,bool endLast){
    if(thd_stt.joinable() == false){
        if(!stt_ok && !endLast) return false;
        else{
            stt_ok = false;
            thd_stt = thread(&Baidu::SpeechToText_Async,this,speechPath);
            thd_stt.detach();
        }
    }else return false;
}
bool Baidu::TextToSpeech_Sync(string text,string savePath,bool endLast){
    if(thd_tts.joinable() == false){
        if(!tts_ok && !endLast) return false;
        else{
            tts_ok = false;
            thd_tts = thread(&Baidu::TextToSpeech_Async,this,text,savePath);
            thd_tts.detach();
        }
    }else return false;
}
void Baidu::Init(){
    this->m_DevPid = getValue("DevPid").asInt();
    this->m_person = getValue("person").asInt();
    this->m_pitch = getValue("pitch").asInt();
    this->m_speed = getValue("speed").asInt();
    this->m_volume = getValue("volume").asInt();
    this->m_audioType = getValue("audioType").asString();
    this->m_rate = getValue("rate").asInt();
}
void Baidu::Update(){
    ShowPanel();
}
void Baidu::PollMessage(Message message){
    if(message.fromType == AgentComponentType::GUI 
        && message.content.at(0) == "InitOK")GUILoadOK = true;
}
void Baidu::ShowPanel(){
    static char inputText[1024] = "你好，我是未来人工智能机器人";
    ImGui::Begin("语音控制台",&ui_show);
    ImGui::InputTextMultiline("输入文本",inputText,sizeof(inputText),ImVec2(-1.0f, ImGui::GetTextLineHeight() * 16));
    if(ImGui::Button("合成",ImVec2(-1.0f, 0.0f))){
        TextToSpeech_Sync(inputText);
    }
    ImGui::InputInt("发音人",&m_person,1,1);
    ImGui::End();
}