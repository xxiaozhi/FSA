#include <config/config.h>
Config::Config(string path,bool endWithSave){
    Json::Reader reader;
    ifstream ifs(path);
    reader.parse(ifs,m_root,false);
    this->endWithSave = endWithSave;
    this->filePath = path;
}
Json::Value Config::getValue(string key){
    return m_root[key];
}
Json::Value Config::getRoot(){
    return m_root;
}
Config::~Config(){
    if(endWithSave){
        ofstream ofs(filePath);
        ofs<<m_root.toStyledString();
    }
}