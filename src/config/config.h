#pragma once
#include <iostream>
#include <fstream>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/json.h>
using namespace std;
class Config{
public:
    Config() = delete;
    ~Config();
    Config(string path,bool endWithSave = false);
    Json::Value getValue(string key);
    Json::Value getRoot();
private:
    Json::Value m_root;
    bool endWithSave = false;
    string filePath;
};