#pragma once
#include <agent/agent.h>
#define IMGUI_IMPL_OPENGL_LOADER_GL3W
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
class Audio : Config , public IAgentComponent{
public:
    Audio();
    void Init();
    void Update();
    void PollMessage(Message message);
    void Play_Async(string filePath);
    bool Play_Sync(string filePath,bool endLast = true);
    void Record_Async(string filePath);
    bool Record_Sync(string filePath = "resource/tmp/rec.wav",bool endLast = true);
    void StartRecord(string filePath = "resource/tmp/rec.wav");
    void EndRecord();
private:
    thread thd_play,thd_record;
    FILE * fp_play = nullptr;
    FILE * fp_record = nullptr;
    bool play_ok = true;
    bool record_ok = true;

    bool panel_show = true;
};