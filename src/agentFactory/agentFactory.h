#pragma once
#include <gui/gui.h>
#include <baidu/baidu.h>
#include <audio/audio.h>
#include <iat/iat.h>
#include <aiui/aiui.h>
#include <weather/weather.h>
#include <mmd/mmd.h>
#include <openAPP/openAPP.h>
#include <smartHome/smartHome.h>

class AgentFactory : Config{
public:
    static AgentFactory * getInstance();
    static shared_ptr<Agent> build();
    AgentFactory();
    ~AgentFactory();
private:
    static AgentFactory * m_instance;
    static map<string,shared_ptr<IAgentComponent> > components;

    vector<string> componentList;
};