#include <agentFactory/agentFactory.h>
AgentFactory * AgentFactory::m_instance = nullptr;
map<string,shared_ptr<IAgentComponent> > AgentFactory::components;
AgentFactory * AgentFactory::getInstance(){
    if(m_instance == nullptr)m_instance = new AgentFactory();
    return m_instance;
}
AgentFactory::AgentFactory():
    Config("resource/config/factory.json"){
    components["BAIDU_SPEECH"] = make_shared<Baidu>();
    components["SOX_AUDIO"] = make_shared<Audio>();
    components["AIUI_NLP"] = make_shared<AIUI>();
    components["IMGUI_GUI"] = make_shared<IMGUI>();
    components["IAT_SPEECH"] = make_shared<Iat>();
    components["WEATHER"] = make_shared<Weather>();
    components["MMD"] = make_shared<AppContext>();
    components["APP"] = make_shared<OpenAPP>();
    components["SmartHome"] = make_shared<SmartHome>();
    Json::Value list = getValue("components");
    int cnt = list.size();
    for(int i=0;i<cnt;++i)
        componentList.push_back(list[i].asString());
}
shared_ptr<Agent> AgentFactory::build(){
    shared_ptr<Agent> agent(new Agent);
    for (auto &&item : getInstance()->componentList)
        agent->AddComponent(item,components[item]);
    
    return agent;
}
AgentFactory::~AgentFactory(){
    components.clear();
}