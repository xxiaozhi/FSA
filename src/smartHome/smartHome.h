#pragma once
#include <agent/agent.h>
#define IMGUI_IMPL_OPENGL_LOADER_GL3W
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <termios.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h> 
class SmartHome: public IAgentComponent,Config{
public:
    SmartHome();
    void PollMessage(Message message);
    void Init();
private:
    void SetLight(bool mode);
    string dev;
    int fd = 0;
private:
    int set_interface_attribs(int fd, int speed);
    void set_mincount(int fd, int mcount);
};