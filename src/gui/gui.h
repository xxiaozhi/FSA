#pragma once
#include <agent/agent.h>
#define IMGUI_IMPL_OPENGL_LOADER_GL3W
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
class IMGUI : public IAgentComponent,Config{
public:
    IMGUI();
    ~IMGUI();
    void Init();
    void Update();
    void BeforeUpdate();
    void AfterUpdate();
private:
    bool showPanel = false;
};