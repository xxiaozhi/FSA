#pragma once
#include <agent/agent.h>
#define IMGUI_IMPL_OPENGL_LOADER_GL3W
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <aiui/AIUI.h>
#include <aiui/FileUtil.h>
using namespace aiui;
class AIUI : public IAgentComponent,Config,public IAIUIListener{
public:
    AIUI();
    ~AIUI(){};
    void Init();
    void Update();
    void PollMessage(Message message);
public:
    void onEvent(const IAIUIEvent& event) const;
    void Wake();
    void WriteText(string text);
private:
    string appid = "5c223f6c";
    IAIUIAgent * aiui_agent = nullptr;
    FileUtil::DataFileHelper * mTtsFileHelper;

    mutable string message_sum;
    bool showPanel = false;
private:
    void ParseNLP(string result) const;
};